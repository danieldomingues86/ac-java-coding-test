package com.mkyong.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import com.starwars.dao.SettingDao;
import com.starwars.dao.SettingDaoImpl;
import com.starwars.model.Settings;

public class UserDaoTest {

    private EmbeddedDatabase db;

    SettingDao settingDao;
    
    @Before
    public void setUp() {
    	db = new EmbeddedDatabaseBuilder()
    		.setType(EmbeddedDatabaseType.H2)
    		.addScript("db/sql/create-db.sql")
    		.addScript("db/sql/insert-data.sql")
    		.build();
    }

    @Test
    public void testFindByname() {
    	NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
    	SettingDaoImpl userDao = new SettingDaoImpl();
    	userDao.setNamedParameterJdbcTemplate(template);
    	
    	Settings settings = settingDao.findById(123);
  
    	Assert.assertNotNull(settings);
    	Assert.assertEquals(1, settings.getId());
    	Assert.assertEquals("mkyong", settings.getName());

    }

    @After
    public void tearDown() {
        db.shutdown();
    }

}