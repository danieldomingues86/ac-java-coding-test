package com.starwars.web.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.starwars.dao.CharacterDao;
import com.starwars.dao.SettingDao;
import com.starwars.dao.WordsDao;
import com.starwars.model.Character;
import com.starwars.model.Settings;
import com.starwars.model.Word;

@RestController
public class UploadFileRESTController {

	private static final Logger logger = LoggerFactory.getLogger(UploadFileRESTController.class);

	@Autowired
	private SettingDao settingDao;

	@Autowired
	private CharacterDao characterDao;

	@Autowired
	private WordsDao wordDao;

	@RequestMapping(value = "/doUpload", method = RequestMethod.POST)
	public String handleFileUpload(HttpServletRequest request, @RequestParam CommonsMultipartFile[] fileUpload)
			throws Exception {

		if (settingDao.findAll().size() > 1) {
			return "Movie script already received";
		}

		try {
			if (fileUpload != null && fileUpload.length > 0) {
				for (CommonsMultipartFile aFile : fileUpload) {
					logger.debug("Processing file: " + aFile.getOriginalFilename());
					Set<Settings> cenas = readContentAndPopulate(aFile.getBytes());

					if (cenas != null) {
						persistInfos(cenas);
					}

				}
			}
		} catch (Exception e) {
			logger.error("upload() failed to save file error: ", e);
			return "Unexpected error";
		}

		return "Movie script successfully received";
	}

	private void persistInfos(Set<Settings> cenas) {

		for (Settings set : cenas) {
			settingDao.insertSetting(set);

			for (Character character : set.getCharacters()) {
				characterDao.insertCharacter(character, set.getId());

				for (Word word : character.getWordCounts()) {
					wordDao.insertWord(word, character.getId());
				}
			}
		}
	}

	private Set<Settings> readContentAndPopulate(byte[] info) {
		InputStream is = null;
		BufferedReader bfReader = null;

		Set<Settings> listaCenas = new HashSet<Settings>();

		try {
			is = new ByteArrayInputStream(info);
			bfReader = new BufferedReader(new InputStreamReader(is));
			String line = null;

			Settings setting = null;
			Character character = null;

			while ((line = bfReader.readLine()) != null) {

				String cena = isSetting(line);
				String personagem = isCharacter(line);
				String[] dialogue = isDialogue(line);

				if (cena != null) {
					setting = new Settings(cena);

					if (!listaCenas.contains(setting)) {
						listaCenas.add(setting);
					}

				}

				if (personagem != null) {
					character = new Character(personagem);
					if (setting != null) {

						if (!setting.getCharacters().contains(personagem)) {
							setting.getCharacters().add(character);
						}

					}
				}

				if (dialogue != null) {

					if (character != null) {
						for (String wordFromDialogue : dialogue) {
							Word word = new Word(wordFromDialogue);
							character.getWordCounts().add(word);
						}
					}
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (Exception ex) {
			}
		}

		return listaCenas;
	}

	int countSpaces(String string) {
		int spaces = 0;
		for (int i = 0; i < string.length(); i++) {
			if (java.lang.Character.isWhitespace(string.charAt(i))) {
				spaces++;
			} else {
				break;
			}
		}
		return spaces;
	}

	private String isSetting(String data) {

		String[] settingParts = data.split("-");

		if (data.startsWith("INT. ") || data.startsWith("EXT. ")) {
			return settingParts[0].substring(5);

		} else if (data.startsWith("INT./EXT. ")) {
			return settingParts[0].substring(10);

		}
		return null;

	}

	private String isCharacter(String data) {

		int spacesNumber = countSpaces(data);

		// its a Character line
		if (spacesNumber == 22) {
			return data.trim();
		}

		return null;
	}

	private String[] isDialogue(String data) {

		int spacesNumber = countSpaces(data);

		// its a dialog line
		if (spacesNumber == 10) {
			return data.trim().split(" ");
		}

		return null;
	}

}