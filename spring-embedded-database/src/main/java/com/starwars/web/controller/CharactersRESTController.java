package com.starwars.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.starwars.dao.CharacterDao;
import com.starwars.model.Character;

@RestController
public class CharactersRESTController {

	@Autowired
	private CharacterDao characterDao;
	
	@RequestMapping(value = "/characters", method = RequestMethod.GET)
	public List<Character> getAllCaracters() {
		return characterDao.findAll();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
	public ResponseEntity<Character> getCharacterById(@PathVariable("id") int id) {
		
		Character character = characterDao.findById(id);
		
		if(character == null){
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Character>(character, HttpStatus.OK);
	}
}