package com.starwars.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.starwars.dao.SettingDao;
import com.starwars.model.Settings;

@RestController
public class SettingsRESTController {

	@Autowired
	private SettingDao settingDao;
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public List<Settings> getAllSettings() {
		return settingDao.findAll();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/settings/{id}", method = RequestMethod.GET)
	public ResponseEntity<Settings> getSettingById(@PathVariable("id") int id) {
		
		Settings settings = settingDao.findById(id);
		
		if(settings == null){
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Settings>(settings, HttpStatus.OK);
	}
}