package com.starwars.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.starwars.model.Word;


@Repository
public class WordsDaoImpl implements WordsDao  {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public Word findById(int id) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        
		String sql = "SELECT * FROM Words WHERE id=:id";
		
		Word result = namedParameterJdbcTemplate.queryForObject(sql, params, new WordMapper());
                    
        return result;
        
	}
	
	@Override
	public List<Word> findAllWordsByCharacterId(int idCharacter) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("idCharacter", idCharacter);
        
		String sql = "SELECT * FROM Words WHERE idCharacter=:idCharacter";
		
		List<Word> result = new ArrayList<Word>();
		
		List<Map<String, Object>> lista = namedParameterJdbcTemplate.queryForList(sql, params);
		
		for (Map<String, Object> map : lista) {
			Word w = new Word("");
			w.setId((Integer)map.get("id"));
			w.setWord((String)map.get("word"));
			result.add(w);
		}
		
        return result;
        
	}

	@Override
	public List<Word> findAll() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT * FROM Words";
		
        List<Word> result = namedParameterJdbcTemplate.query(sql, params, new WordMapper());
        
        return result;
        
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void insertWord(Word word, int idCharacter) {
		String query = "INSERT INTO Words (id, word, idCharacter) VALUES (:id,:word,:idCharacter)";
		Map namedParameters = new HashMap();
		namedParameters.put("id", word.getId());
		namedParameters.put("word", word.getWord());
		namedParameters.put("idCharacter", idCharacter);
		namedParameterJdbcTemplate.update(query, namedParameters);
	}

	private static final class WordMapper implements RowMapper<Word> {

		public Word mapRow(ResultSet rs, int rowNum) throws SQLException {
			Word Word = new Word(rs.getString("name"));
			return Word;
		}
	}


}
