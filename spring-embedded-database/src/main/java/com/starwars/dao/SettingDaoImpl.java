package com.starwars.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.starwars.model.Character;
import com.starwars.model.Settings;
import com.starwars.model.Word;

@Repository
public class SettingDaoImpl implements SettingDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Settings findById(int id) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "select s.id, s.name, c.id, c.name, w.id, w.word " + "from Settings s "
				+ "INNER join Character c ON s.id = c.idSetting " + "INNER join Words w ON c.id = w.idCharacter "
				+ "WHERE s.id=:id";

		List<Settings> result = namedParameterJdbcTemplate.query(sql, params, new SettingsMapper());

		if(result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<Settings> findAll() {

		String sql = "select s.id, s.name, c.id, c.name, w.id, w.word " + "from Settings s "
				+ "INNER join Character c ON s.id = c.idSetting " + "INNER join Words w ON c.id = w.idCharacter ";

		List<Settings> result = namedParameterJdbcTemplate.query(sql, new SettingsMapper());

		return result;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void insertSetting(Settings setting) {

		String query = "INSERT INTO Settings (id, name) VALUES (:id, :name)";
		Map namedParameters = new HashMap();
		namedParameters.put("id", setting.getId());
		namedParameters.put("name", setting.getName());
		namedParameterJdbcTemplate.update(query, namedParameters);
	}

	private static final class SettingsMapper implements RowMapper<Settings> {

		private String lastSceneName = "";
		private String lastChararacterName = "";
		private String lastWord = "";
		
		private int currentSceneId;
		private String currentSceneName;
		private int currentCharacterId;
		private String currentChararacterName;
		private int currentWordId;
		private String currentWord;
		
		private Settings setting = new Settings("");
		private Character character = new Character("");

		public Settings mapRow(ResultSet rs, int rowNum) throws SQLException {

			currentSceneId = rs.getInt(1);
			currentSceneName = rs.getString(2);
			currentCharacterId = rs.getInt(3);
			currentChararacterName = rs.getString(4);
			currentWordId = rs.getInt(5);
			currentWord = rs.getNString(6);
			
			if(!currentSceneName.equals(lastSceneName)){
				setting = new Settings("");
				setting.setId(currentSceneId);
				setting.setName(currentSceneName);
			}
			

			//dentro da cena verifica se eh o mesmo personagem
			if(!currentChararacterName.equals(lastChararacterName)){
				character = new Character("");
				character.setId(currentCharacterId);
				character.setName(currentChararacterName);
				setting.getCharacters().add(character);
			}
			
			
			//dentro do personagem verifica se eh a mesma palavra dita
			if(!currentWord.equals(lastWord)){
				Word word = new Word("");
				word.setId(currentWordId);
				word.setWord(currentWord);
				character.getWordCounts().add(word);
			}
			
			//atualiza ultimas informacoes
			lastSceneName = rs.getString(2);
			lastChararacterName = rs.getString(4);
			lastWord = rs.getNString(6);
			
			return setting;
		}
	}

}