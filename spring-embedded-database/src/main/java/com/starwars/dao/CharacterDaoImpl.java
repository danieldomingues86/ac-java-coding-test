package com.starwars.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.starwars.model.Character;
import com.starwars.model.Word;

@Repository
public class CharacterDaoImpl implements CharacterDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public Character findById(int id) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        
		String sql = "select c.id, c.name, w.id, w.word "
				+ "from Character c INNER join Words w "
				+ "ON c.id = w.idCharacter WHERE c.id=:id";
		
		List<Character> result = namedParameterJdbcTemplate.query(
                    sql,
                    params,
                    new CharacterMapper());
                    
		if(result.size() > 0){
			return result.get(0);
		}else{
			return null;
		}
	}
	

	@Override
	public List<Character> findAll() {
		String sql = "select c.id, c.name, w.id, w.word "
				+ "from Character c INNER join Words w "
				+ "ON c.id = w.idCharacter ";
	
		List<Character> result = namedParameterJdbcTemplate.query(sql, new CharacterMapper());
        
        return result;
        
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void insertCharacter(Character character, int idSetting) {
		String query = "INSERT INTO Character (id, name, idSetting) VALUES (:id,:name, :idSetting)";
		Map namedParameters = new HashMap();
		namedParameters.put("id", character.getId());
		namedParameters.put("name", character.getName());
		namedParameters.put("idSetting", idSetting); 
		namedParameterJdbcTemplate.update(query, namedParameters);
	}
	
	
	private static final class CharacterMapper implements RowMapper<com.starwars.model.Character> {

		private String lastChararacterName = "";
		private String lastWord = "";
		
		private int currentCharacterId;
		private String currentChararacterName;
		private int currentWordId;
		private String currentWord;
		
		private Character character = new Character("");
		
		public com.starwars.model.Character mapRow(ResultSet rs, int rowNum) throws SQLException {

			currentCharacterId = rs.getInt(1);
			currentChararacterName = rs.getString(2);
			currentWordId = rs.getInt(3);
			currentWord = rs.getNString(4);
			

			//dentro da cena verifica se eh o mesmo personagem
			if(!currentChararacterName.equals(lastChararacterName)){
				character = new Character("");
				character.setId(currentCharacterId);
				character.setName(currentChararacterName);
			}
			
			
			//dentro do personagem verifica se eh a mesma palavra dita
			if(!currentWord.equals(lastWord)){
				Word word = new Word("");
				word.setId(currentWordId);
				word.setWord(currentWord);
				character.getWordCounts().add(word);
			}
			
			//atualiza ultimas informacoes
			lastChararacterName = rs.getString(2);
			lastWord = rs.getNString(4);
			
	        return character;
		}
	}

}