package com.starwars.model;

import java.util.ArrayList;
import java.util.List;

public class Character {

	private static int countChars = 0;

	private int id;
	private String name;
	private List<Word> wordCounts = new ArrayList<Word>();

	public Character(String name) {
		this.name = name;
		this.id = countChars++;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public List<Word> getWordCounts() {
		return wordCounts;
	}
	
	public void setWordCounts(List<Word> wordCounts) {
		this.wordCounts = wordCounts;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Character other = (Character) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


}
