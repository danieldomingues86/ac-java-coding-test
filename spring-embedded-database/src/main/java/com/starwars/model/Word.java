package com.starwars.model;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties("id")   
public class Word {

	private static final AtomicInteger countWords = new AtomicInteger(0); 
	
	private int id; 
	
    private String word;
    
    public Word(String word) {
    	this.word = word;
    	id = countWords.incrementAndGet(); 
	}
    
    public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getWord() {
		return word;
	}
	
	public void setWord(String word) {
		this.word = word;
	}

}
