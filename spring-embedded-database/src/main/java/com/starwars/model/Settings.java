package com.starwars.model;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Settings {

	private static final AtomicInteger countSettings = new AtomicInteger(0); 
   
	private static Settings lastSettingsFound;
	
	private int id; 
	private String name;
    private Set<Character> characters = new HashSet<Character>();
    
    public Settings(String name) {
    	this.name = name;
    	id = countSettings.incrementAndGet(); 
	}
    
    public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Character> getCharacters() {
		return characters;
	}

	public static Settings getLastSettingsFound() {
		return lastSettingsFound;
	}
	
	public void setCharacters(Set<Character> characters) {
		this.characters = characters;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Settings other = (Settings) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
	
	
}
