
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body bgcolor="#868d89">
	<h1>Welcome to AvenueCode - StarWars REST Java application</h1>

	<p>This is a web based REST application that reads a script storages its informations in a in-memory database
	and using Rest Resquests its possible to retrieve informations.</p>


	<img src="<%=request.getContextPath() %>/resources/images/img1.jpg"/>
	
	
	<br>
	
	<h3>Instructions - How to use this application:</h3>
	<h3>You can use the following Requests to interact with this application</h3>
	<p>POST - http://localhost:8080/spring-mvc-db/uploadPage</p>
	<p>GET - http://localhost:8080/spring-mvc-db/settings </p>
	<p>GET - http://localhost:8080/spring-mvc-db/settings/id</p>
	<p>GET - http://localhost:8080/spring-mvc-db/characters</p>
	<p>GET - http://localhost:8080/spring-mvc-db/characters/id</p>
	

	<form action="<c:url value="/uploadPage" />" method="GET">
		<input type="submit" name="action" value="First add your Script Movie" />
	</form>

</body>
</html>
